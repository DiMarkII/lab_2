#include "CppUnitTest.h"
#include "main.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace tests
{
    TEST_CLASS(tests){
    public :
        TEST_METHOD(test_compression)
        {
            string input = "Hello!";
            string encoded;
            string true_answer = "10011100110101";
            encode(input, encoded);
            Assert::IsTrue(encoded == true_answer);
        }
        TEST_METHOS(test_decompression)
        {
            string input = "Hello!";
            string encoded;
            auto codes = encode(input, encoded);
            string decoded = decode(encoded, codes);
            Assert::IsTrue(decoded == input);
        }
    };
}