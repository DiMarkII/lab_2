#include <iostream>
#include <string>
#include <algorithm>
#include <iomanip>
#include <cassert>

#include "tree.h"

using namespace std;

struct pnode
{
    char ch; // char
    int num; // probability
    bool operator<(const pnode &sec) {return num > sec.num;}
};

struct treenode : public pnode
{
    char lcode;
    char rcode;
    treenode *left;  // left child
    treenode *right; // right child
    ~treenode()
    {
        if (left)
            delete left;
        if (right)
            delete right;
    }
};

void GenerateCode(treenode *node, tree<char, string> &codes);
tree<char, string> get_codes(pnode *ptable, int tsize);
tree<char, string> encode(string &input, string &output);
pnode* get_ptable(tree<char, int> &freqs, int &tsize);
tree<char, int> get_freqs(string &input);
string decode(string input, tree<char, std::string> &codes);

int main()
{
    string input;
    std::cout << "Write your string:\n";
    std::getline(std::cin, input);
    string encoded, decoded;
    auto codes = encode(input, encoded);
    decoded = decode(encoded, codes);

    std::cout << "\nOutput:\n" << encoded << '\n';
    std::cout << "Compress ratio input/output = " << input.size()*8.0 / encoded.size() << '\n';
    std::cout << "\nDecompressed:\n" << decoded << '\n';

    assert(input == decoded);

    return 0;
}

void GenerateCode(treenode *node, tree<char, string> &codes)
{
    static string sequence = "";
    if (node->left)
    {
        sequence += node->lcode;
        GenerateCode(node->left, codes);
    }

    if (node->right)
    {
        sequence += node->rcode;
        GenerateCode(node->right, codes);
    }

    if (!node->left && !node->right)
    {
        string *temp = codes.find(node->ch);
        if (temp)
            *temp = sequence;
        else
            codes.insert(node->ch, sequence);
    }

    int l = sequence.length();
    if (l > 1)
        sequence = sequence.substr(0, l - 1);
    else
        sequence = "";
}

tree<char, string> get_codes(pnode *ptable, int tsize)
{
    tree<char, string> codes;
    treenode **tops = new treenode*[tsize];
    int top_size = tsize;
    for (int i = 0; i < top_size; i++)
    {
        treenode *temp = new treenode;
        temp->ch = ptable[i].ch;
        temp->num = ptable[i].num;
        temp->left = NULL;
        temp->right = NULL;
        tops[i] = temp;
    }

    while (top_size > 1)
    {
        treenode *temp = new treenode;
        temp->num = tops[top_size - 2]->num + tops[top_size - 1]->num;
        temp->left = tops[top_size - 2];
        temp->right = tops[top_size - 1];
        if (temp->left->num < temp->right->num)
        {
            temp->lcode = '0';
            temp->rcode = '1';
        }
        else
        {
            temp->lcode = '1';
            temp->rcode = '0';
        }
        top_size -= 2;
        bool is_insert = false;
        for (int i = 0; i < top_size; ++i)
            if (tops[i]->num < temp->num)
            {
                for (int j = top_size - 1; j >= i; --j)
                    tops[j + 1] = tops[j];
                tops[i] = temp;
                ++top_size;
                is_insert = true;
                break;
            }
        if (!is_insert)
        {
            tops[top_size] = temp;
            ++top_size;
        }
    }

    GenerateCode(tops[0], codes);

    delete tops[0];
    delete[] tops;
    return codes;
}

tree<char, string> encode(string &input, string &output)
{
    auto freqs = get_freqs(input);
    int tsize;
    auto ptable = get_ptable(freqs, tsize);
    auto codes = get_codes(ptable, tsize);
    std::cout << "\nsize = " << tsize << '\n';
    int width = 6;
    std::cout << setw(width) << "char" << setw(width) << "num" << setw(width) << "code" << '\n';
    for (int i = 0; i < tsize; ++i)
    {
        string *code = codes.find(ptable[i].ch);
        if (code)
            std::cout << setw(width) << ptable[i].ch << setw(width) << ptable[i].num << setw(width) << *codes.find(ptable[i].ch) << '\n';
        else
            std::cout << "Error on " << ptable[i].ch  << '\n';
    }
    delete[] ptable;
    output.clear();
    for (int i = 0; i < input.size(); ++i)
        output += *codes.find(input[i]);

    return codes;
}

string decode(string input, tree<char, std::string> &codes)
{
    string output;
    string accum = "";
    auto keys = codes.get_keys();
    auto values = codes.get_values();
    for (int i = 0; i < input.size(); ++i)
    {
        accum += input[i];
        //std::cout << "accum = " << accum << '\n';
        for (int j = 0; j < values.size(); ++j)
            if (accum == values[j])
            {
                accum = "";
                output += keys[j];
            }
    }
    return output;
}

tree<char, int> get_freqs(string &input)
{
    tree<char, int> result;
    for (int i = 0; i < input.size(); ++i)
    {
        int *temp = result.find(input[i]);
        if (temp)
            ++*temp;
        else
            result.insert(input[i], 1);
    }
    return result;
}

pnode* get_ptable(tree<char, int> &freqs, int &tsize)
{
    auto keys = freqs.get_keys();
    auto values = freqs.get_values();
    tsize = keys.size();
    pnode* ptable = new pnode[tsize];
    for (int i = 0; i < tsize; ++i)
    {
        ptable[i].ch = keys[i];
        ptable[i].num = values[i];
    }
    sort(ptable, ptable + tsize);
    return ptable;
}